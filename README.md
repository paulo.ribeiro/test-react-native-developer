# Test for React Native Developer

> [![Smartex Ai](https://www.smartex.ai/wp-content/uploads/2020/10/Logotipo_smartex.png)](https://www.smartex.ai)
>
>
> This test is designed to test your knowledge in cross-platform mobile application development using React Native.

### Requirements
- React Native (https://facebook.github.io/react-native/)
- React (https://facebook.github.io/react/)
- Material Design (https://material.io/guidelines/)
- Native Components
- JSX
- Local database (eg. sqlite)

### Desirable
- Unit tests

## How to participate?
> Create a repository and after finishing the test invete paulo.ribeiro@smartex.ai, shrujal.shah@smartex.ai and joao.mesquita@smartex.ai to be analyzed by us. :D

## Test details
> The test consists of creating a simple To Do List app. <br>

### Functionalities
> The app must contain the following features:

1. Navigation with User Data and Menu Items
1. Task List
2. Creating a new task
4. Task Editing and Deleting
5. Display delete confirmation alert

*Note: It is not mandatory to create a login page, but if you want it will be considered a differential* <br>
*Obs².: It is not mandatory to faithfully follow the colors and icons of the prototype, but it is important to try to follow the same structure*



### App Mockup
> Click to enlarge the images <br>

[![Menu](http://i.imgur.com/U443Ore.jpg)](http://i.imgur.com/Zpj5lwj.png)
[![List](http://i.imgur.com/Eb88PkA.jpg)](http://i.imgur.com/0zihnYm.png)
[![Create](http://i.imgur.com/KacMBSo.jpg)](http://i.imgur.com/6Fb53k7.png)
[![Edit](http://i.imgur.com/Wf478tT.jpg)](http://i.imgur.com/gL8OMVF.png)


### Thanks in advance and good luck! ;)
